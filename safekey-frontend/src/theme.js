import { createTheme } from '@mui/material/styles';
import { purple } from '@mui/material/colors';

const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#1976d2', // ChatGPT-like primary color
    },
    secondary: {
      main: purple[500], // Purple accents
    },
    background: {
      default: '#121212', // Dark background similar to ChatGPT or Obsidian
      paper: '#1d1d1d', // Slightly lighter dark background for paper components
    },
    text: {
      primary: '#ffffff', // White text for better contrast on dark background
      secondary: '#b0b0b0', // Grey text for secondary content
    },
  },
  typography: {
    h4: {
      fontWeight: 600,
      color: '#ffffff', // White text for headers
    },
    h5: {
      fontWeight: 500,
      color: '#ffffff', // White text for headers
    },
    body1: {
      fontSize: '1rem',
      color: '#b0b0b0', // Grey text for body
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: '8px', // ChatGPT-like rounded buttons
        },
      },
    },
  },
});

export default theme;
