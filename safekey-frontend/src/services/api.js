import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:8000/api/',
});

// Add a request interceptor
api.interceptors.request.use(async (config) => {
  let token = localStorage.getItem('access');
  if (token) {
    const expiration = JSON.parse(atob(token.split('.')[1])).exp;
    if (Date.now() >= expiration * 1000) {
      // Token expired, refresh it
      const refreshToken = localStorage.getItem('refresh');
      try {
        const response = await axios.post('http://localhost:8000/api/token/refresh/', { refresh: refreshToken });
        token = response.data.access;
        localStorage.setItem('access', token);
      } catch (error) {
        console.error('Failed to refresh token', error);
        localStorage.removeItem('access');
        localStorage.removeItem('refresh');
      }
    }
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;
