import React, { useState } from 'react';
import api from '../services/api';

const ModifyPassword = ({ passwordId }) => {
  const [label, setLabel] = useState('');
  const [encryptedPassword, setEncryptedPassword] = useState('');

  const modifyPassword = async () => {
    try {
      const response = await api.post(`modify-password/${passwordId}/`, {
        label,
        encrypted_password: encryptedPassword,
      });
      alert('Password modified successfully!');
    } catch (error) {
      console.error('Error modifying password:', error);
    }
  };

  return (
    <div>
      <h2>Modify Password</h2>
      <input
        type="text"
        placeholder="Label"
        value={label}
        onChange={(e) => setLabel(e.target.value)}
      />
      <input
        type="password"
        placeholder="New Password"
        value={encryptedPassword}
        onChange={(e) => setEncryptedPassword(e.target.value)}
      />
      <button onClick={modifyPassword}>Modify</button>
    </div>
  );
};

export default ModifyPassword;
