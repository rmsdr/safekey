import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../services/api';
import SearchBar from './SearchBar';
import CryptoJS from 'crypto-js';
import {
  Container,
  TextField,
  Button,
  Typography,
  List,
  ListItem,
  ListItemText,
  IconButton,
  Box,
  Paper,
} from '@mui/material';
import { Add, Edit, Cancel, Save, Visibility, VisibilityOff, ExitToApp } from '@mui/icons-material';

// Use a predefined key for encryption/decryption (in a real application, this key should be stored securely)
const ENCRYPTION_KEY = 'your-encryption-key';

const PasswordManager = () => {
  const [passwords, setPasswords] = useState([]);
  const [label, setLabel] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [query, setQuery] = useState('');
  const [editingPasswordId, setEditingPasswordId] = useState(null);
  const [visiblePasswordId, setVisiblePasswordId] = useState(null);
  const navigate = useNavigate();

  const fetchPasswords = async () => {
    try {
      const response = await api.get('passwords/');
      setPasswords(response.data);
    } catch (err) {
      setError('Failed to fetch passwords');
    }
  };

  useEffect(() => {
    fetchPasswords();
  }, []);

  const encryptPassword = (plainTextPassword) => {
    return CryptoJS.AES.encrypt(plainTextPassword, ENCRYPTION_KEY).toString();
  };

  const decryptPassword = (encryptedPassword) => {
    const bytes = CryptoJS.AES.decrypt(encryptedPassword, ENCRYPTION_KEY);
    return bytes.toString(CryptoJS.enc.Utf8);
  };

  const handleAddPassword = async (e) => {
    e.preventDefault();
    const encryptedPassword = encryptPassword(password);
    try {
      const response = await api.post('passwords/', {
        label,
        encrypted_password: encryptedPassword,
      });
      setPasswords([...passwords, response.data]);
      setLabel('');
      setPassword('');
    } catch (err) {
      setError('Failed to add password');
    }
  };

  const handleGeneratePassword = async () => {
    try {
      const response = await api.get('generate-password/');
      setPassword(response.data.password);
    } catch (err) {
      setError('Failed to generate password');
    }
  };

  const handleModifyPassword = async (e) => {
    e.preventDefault();
    const encryptedPassword = encryptPassword(password);
    try {
      const response = await api.patch(`modify-password/${editingPasswordId}/`, {
        label,
        encrypted_password: encryptedPassword,
      });
      setPasswords(
        passwords.map((pw) =>
          pw.id === editingPasswordId ? response.data : pw
        )
      );
      setLabel('');
      setPassword('');
      setEditingPasswordId(null);
    } catch (err) {
      setError('Failed to modify password');
    }
  };

  const handlePasswordClick = (password) => {
    setEditingPasswordId(password.id);
    setLabel(password.label);
    setPassword('');  // Do not pre-fill the password field with actual password
  };

  const togglePasswordVisibility = (id) => {
    if (visiblePasswordId === id) {
      setVisiblePasswordId(null);
    } else {
      setVisiblePasswordId(id);
    }
  };

  const handleLogout = () => {
    localStorage.removeItem('access');
    localStorage.removeItem('refresh');
    navigate('/register');
  };

  const filteredPasswords = passwords.filter((pw) =>
    pw.label.toLowerCase().includes(query.toLowerCase())
  );

  return (
    <Container maxWidth="md">
      <Paper elevation={3} style={{ padding: '2rem', backgroundColor: '#1d1d1d' }}>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography variant="h4" component="h2" gutterBottom>
            Password Manager
          </Typography>
          <Button
            variant="contained"
            color="secondary"
            startIcon={<ExitToApp />}
            onClick={handleLogout}
          >
            Disconnect
          </Button>
        </Box>
        <SearchBar query={query} setQuery={setQuery} />
        <form onSubmit={editingPasswordId ? handleModifyPassword : handleAddPassword}>
          <Box mb={2}>
            <TextField
              label="Label"
              variant="outlined"
              fullWidth
              value={label}
              onChange={(e) => setLabel(e.target.value)}
              margin="normal"
              InputLabelProps={{
                style: { color: '#b0b0b0' }, // Label color
              }}
            />
            <TextField
              label="Password"
              type="password"
              variant="outlined"
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              margin="normal"
              InputLabelProps={{
                style: { color: '#b0b0b0' }, // Label color
              }}
            />
            <Button variant="contained" color="primary" onClick={handleGeneratePassword} startIcon={<Add />}>
              Generate
            </Button>
          </Box>
          {error && <Typography color="error">{error}</Typography>}
          <Button variant="contained" color="primary" type="submit" startIcon={editingPasswordId ? <Save /> : <Add />}>
            {editingPasswordId ? 'Modify Password' : 'Add Password'}
          </Button>
          {editingPasswordId && (
            <Button variant="contained" color="secondary" onClick={() => { setEditingPasswordId(null); setLabel(''); setPassword(''); }} startIcon={<Cancel />}>
              Cancel
            </Button>
          )}
        </form>
        <Typography variant="h5" component="h3" gutterBottom>
          Stored Passwords
        </Typography>
        <List>
          {filteredPasswords.map((pw) => (
            <ListItem key={pw.id}>
              <ListItemText primary={pw.label} />
              {visiblePasswordId === pw.id ? (
                <Typography variant="body1">{decryptPassword(pw.encrypted_password)}</Typography>
              ) : (
                <Typography variant="body1">••••••••</Typography>
              )}
              <IconButton edge="end" aria-label="visibility" onClick={() => togglePasswordVisibility(pw.id)}>
                {visiblePasswordId === pw.id ? <VisibilityOff /> : <Visibility />}
              </IconButton>
              <IconButton edge="end" aria-label="edit" onClick={() => handlePasswordClick(pw)}>
                <Edit />
              </IconButton>
            </ListItem>
          ))}
        </List>
      </Paper>
    </Container>
  );
};

export default PasswordManager;
