import React, { useState } from 'react';
import api from '../services/api';

const TOTPVerify = () => {
  const [token, setToken] = useState('');
  const [message, setMessage] = useState('');
  const [error, setError] = useState('');

  const handleVerify = async (e) => {
    e.preventDefault();
    try {
      const response = await api.post('totp/verify/', { token });
      setMessage(response.data.detail);
      setError('');
    } catch (err) {
      setError('Invalid TOTP token');
      setMessage('');
    }
  };

  return (
    <div>
      <h2>TOTP Verify</h2>
      {error && <p>{error}</p>}
      {message && <p>{message}</p>}
      <form onSubmit={handleVerify}>
        <div>
          <label>TOTP Token</label>
          <input
            type="text"
            value={token}
            onChange={(e) => setToken(e.target.value)}
          />
        </div>
        <button type="submit">Verify</button>
      </form>
    </div>
  );
};

export default TOTPVerify;
