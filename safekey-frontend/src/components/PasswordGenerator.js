import React, { useState, useEffect } from 'react';
import api from '../services/api';
import SearchBar from './SearchBar';

const PasswordManager = () => {
  const [passwords, setPasswords] = useState([]);
  const [label, setLabel] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [query, setQuery] = useState('');
  const [editingPasswordId, setEditingPasswordId] = useState(null);

  const fetchPasswords = async () => {
    try {
      const response = await api.get('passwords/');
      setPasswords(response.data);
    } catch (err) {
      setError('Failed to fetch passwords');
    }
  };

  useEffect(() => {
    fetchPasswords();
  }, []);

  const handleAddPassword = async (e) => {
    e.preventDefault();
    try {
      const response = await api.post('passwords/', {
        label,
        encrypted_password: password,
      });
      setPasswords([...passwords, response.data]);
      setLabel('');
      setPassword('');
    } catch (err) {
      setError('Failed to add password');
    }
  };

  const handleGeneratePassword = async () => {
    try {
      const response = await api.get('generate-password/', {
        params: { length: 12 },
      });
      setPassword(response.data.password);
    } catch (err) {
      setError('Failed to generate password');
    }
  };

  const handleModifyPassword = async (e) => {
    e.preventDefault();
    try {
      const response = await api.post(`modify-password/${editingPasswordId}/`, {
        label,
        encrypted_password: password,
      });
      setPasswords(
        passwords.map((pw) =>
          pw.id === editingPasswordId ? response.data : pw
        )
      );
      setLabel('');
      setPassword('');
      setEditingPasswordId(null);
    } catch (err) {
      setError('Failed to modify password');
    }
  };

  const handlePasswordClick = (password) => {
    setEditingPasswordId(password.id);
    setLabel(password.label);
    setPassword(password.encrypted_password);
  };

  const filteredPasswords = passwords.filter((pw) =>
    pw.label.toLowerCase().includes(query.toLowerCase())
  );

  return (
    <div>
      <h2>Password Manager</h2>
      <SearchBar query={query} setQuery={setQuery} />
      <form onSubmit={editingPasswordId ? handleModifyPassword : handleAddPassword}>
        <div>
          <label>Label</label>
          <input
            type="text"
            value={label}
            onChange={(e) => setLabel(e.target.value)}
          />
        </div>
        <div>
          <label>Password</label>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <button type="button" onClick={handleGeneratePassword}>Generate</button>
        </div>
        {error && <p>{error}</p>}
        <button type="submit">{editingPasswordId ? 'Modify Password' : 'Add Password'}</button>
      </form>
      <h3>Stored Passwords</h3>
      <ul>
        {filteredPasswords.map((pw) => (
          <li key={pw.id} onClick={() => handlePasswordClick(pw)}>
            {pw.label}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default PasswordManager;
