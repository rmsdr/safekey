import React from 'react';
import { TextField, Box } from '@mui/material';

const SearchBar = ({ query, setQuery }) => {
  return (
    <Box mb={2}>
      <TextField
        label="Search"
        variant="outlined"
        fullWidth
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        margin="normal"
      />
    </Box>
  );
};

export default SearchBar;
