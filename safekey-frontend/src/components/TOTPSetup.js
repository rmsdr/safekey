import React, { useState, useEffect } from 'react';
import api from '../services/api';
import QRCode from 'qrcode.react';

const TOTPSetup = () => {
  const [totpUrl, setTotpUrl] = useState('');
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchTOTP = async () => {
      try {
        const response = await api.get('totp/setup/');
        console.log(response.data); // Debug: Log response data
        setTotpUrl(response.data.totp_url);
      } catch (err) {
        console.error(err); // Debug: Log error
        setError('Failed to fetch TOTP setup');
      }
    };

    fetchTOTP();
  }, []);

  return (
    <div>
      <h2>TOTP Setup</h2>
      {error && <p>{error}</p>}
      {totpUrl ? (
        <div>
          <QRCode value={totpUrl} />
          <p>Scan the QR code with your TOTP app (e.g., Google Authenticator).</p>
          <p>If you cannot scan the QR code, you can manually enter the following URL in your TOTP app:</p>
          <p>{totpUrl}</p>
        </div>
      ) : (
        <p>Loading QR code...</p>
      )}
    </div>
  );
};

export default TOTPSetup;
