import React from 'react';
import { ThemeProvider, CssBaseline } from '@mui/material';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import theme from './theme';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import TotpSetupPage from './pages/TOTPSetupPage';
import TotpVerifyPage from './pages/TOTPVerifyPage';
import PasswordManager from './pages/PasswordManagerPage';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Routes>
          <Route path="/" element={<RegisterPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/totp-setup" element={<TotpSetupPage />} />
          <Route path="/totp-verify" element={<TotpVerifyPage />} />
          <Route path="/password-manager" element={<PasswordManager />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
};

export default App;
