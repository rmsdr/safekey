import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../services/api';
import { Container, TextField, Button, Typography, Box, Paper } from '@mui/material';

const TotpVerifyPage = () => {
  const [token, setToken] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleVerify = async (e) => {
    e.preventDefault();
    try {
      const response = await api.post('totp-verify/', { token });
      if (response.status === 200) {
        navigate('/password-manager');
      } else {
        setError('Invalid TOTP token');
      }
    } catch (err) {
      setError('Failed to verify TOTP');
    }
  };

  return (
    <Container maxWidth="sm">
      <Paper elevation={3} style={{ padding: '2rem', backgroundColor: '#1d1d1d' }}>
        <Box my={4}>
          <Typography variant="h4" component="h2" gutterBottom>
            Verify TOTP
          </Typography>
          <form onSubmit={handleVerify}>
            <TextField
              label="TOTP Token"
              variant="outlined"
              fullWidth
              value={token}
              onChange={(e) => setToken(e.target.value)}
              margin="normal"
              InputLabelProps={{
                style: { color: '#b0b0b0' }, // Label color
              }}
            />
            {error && <Typography color="error">{error}</Typography>}
            <Box mt={2}>
              <Button variant="contained" color="primary" type="submit" fullWidth>
                Verify
              </Button>
            </Box>
          </form>
        </Box>
      </Paper>
    </Container>
  );
};

export default TotpVerifyPage;
