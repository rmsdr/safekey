import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../services/api';
import { Container, Button, Typography, Box, Paper } from '@mui/material';
import QRCode from 'qrcode.react';

const TotpSetupPage = () => {
  const [totpUrl, setTotpUrl] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const fetchTotpUrl = async () => {
      try {
        const response = await api.get('totp-setup/');
        setTotpUrl(response.data.totp_url);
      } catch (err) {
        console.error('Failed to fetch TOTP URL', err);
      }
    };

    fetchTotpUrl();
  }, []);

  return (
    <Container maxWidth="sm">
      <Paper elevation={3} style={{ padding: '2rem', backgroundColor: '#1d1d1d' }}>
        <Box my={4}>
          <Typography variant="h4" component="h2" gutterBottom>
            Set Up TOTP
          </Typography>
          {totpUrl && <QRCode value={totpUrl} />}
          <Box mt={2}>
            <Button variant="contained" color="primary" onClick={() => navigate('/login')} fullWidth>
              Back to Login
            </Button>
          </Box>
        </Box>
      </Paper>
    </Container>
  );
};

export default TotpSetupPage;
