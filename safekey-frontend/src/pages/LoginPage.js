import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../services/api';
import { Container, TextField, Button, Typography, Box, Paper } from '@mui/material';

const LoginPage = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const response = await api.post('token/', { username, password });
      localStorage.setItem('access', response.data.access);
      localStorage.setItem('refresh', response.data.refresh);
      navigate('/totp-verify');
    } catch (err) {
      setError('Failed to login');
    }
  };

  return (
    <Container maxWidth="sm">
      <Paper elevation={3} style={{ padding: '2rem', backgroundColor: '#1d1d1d' }}>
        <Box my={4}>
          <Typography variant="h4" component="h2" gutterBottom>
            Login
          </Typography>
          <form onSubmit={handleLogin}>
            <TextField
              label="Username"
              variant="outlined"
              fullWidth
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              margin="normal"
              InputLabelProps={{
                style: { color: '#b0b0b0' }, // Label color
              }}
            />
            <TextField
              label="Password"
              type="password"
              variant="outlined"
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              margin="normal"
              InputLabelProps={{
                style: { color: '#b0b0b0' }, // Label color
              }}
            />
            {error && <Typography color="error">{error}</Typography>}
            <Box mt={2}>
              <Button variant="contained" color="primary" type="submit" fullWidth>
                Login
              </Button>
            </Box>
          </form>
        </Box>
      </Paper>
    </Container>
  );
};

export default LoginPage;
