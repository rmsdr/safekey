import React from 'react';
import PasswordManager from '../components/PasswordManager';

const PasswordManagerPage = () => {
  return (
    <div>
      <PasswordManager />
    </div>
  );
};

export default PasswordManagerPage;
