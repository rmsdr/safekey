from django.urls import path, re_path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.routers import DefaultRouter
from .views import (
    RegisterView,
    UserListView,
    PasswordViewSet,
    GeneratePasswordView,
    ModifyPasswordView,
    TOTPSetupView,
    TOTPVerifyView,
    ExportPasswordsView,
    ImportPasswordsView,
)

schema_view = get_schema_view(
    openapi.Info(
        title="SafeKey API",
        default_version="v1",
        description="API documentation for SafeKey project",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@safekey.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register(r"passwords", PasswordViewSet, basename="password")

urlpatterns = [
    path("register/", RegisterView.as_view(), name="register"),
    path("users/", UserListView.as_view(), name="user-list"),
    path("totp-setup/", TOTPSetupView.as_view(), name="totp_setup"),
    path("totp-verify/", TOTPVerifyView.as_view(), name="totp_verify"),
    path("export/", ExportPasswordsView.as_view(), name="export-passwords"),
    path("import/", ImportPasswordsView.as_view(), name="import-passwords"),
    path(
        "generate-password/",
        GeneratePasswordView.as_view(),
        name="generate_password",
    ),
    path(
        "modify-password/<uuid:pk>/",
        ModifyPasswordView.as_view(),
        name="modify_password",
    ),
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path("", include(router.urls)),
]
