from django.db import models
from cryptography.fernet import Fernet
from django.conf import settings


class EncryptedField(models.TextField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cipher_suite = Fernet(settings.ENCRYPTION_KEY)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return self.cipher_suite.decrypt(value.encode()).decode()

    def to_python(self, value):
        if value is None:
            return value
        if isinstance(value, bytes):
            return self.cipher_suite.decrypt(value).decode()
        if isinstance(value, str):
            return value
        return str(value)

    def get_prep_value(self, value):
        if value is None:
            return value
        return self.cipher_suite.encrypt(value.encode()).decode()
