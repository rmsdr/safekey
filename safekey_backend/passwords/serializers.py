from rest_framework import serializers
from .models import Password
from django.contrib.auth.models import User


class PasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Password
        fields = [
            "id",
            "user",
            "label",
            "encrypted_password",
            "created_at",
            "updated_at",
        ]
        read_only_fields = ["id", "created_at", "updated_at"]


class PasswordCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Password
        fields = ["label", "encrypted_password"]


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email", "password")
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username", "email", "is_active", "date_joined"]
