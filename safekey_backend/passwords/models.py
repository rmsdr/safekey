from django.db import models
from django.contrib.auth.models import User
from .fields import EncryptedField
import uuid


class UUIDModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        abstract = True


class Password(UUIDModel):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="password_entries"
    )
    label = models.CharField(max_length=100)
    encrypted_password = EncryptedField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label
