from rest_framework import viewsets, permissions, generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from .models import Password
from .serializers import (
    PasswordSerializer,
    PasswordCreateSerializer,
    UserSerializer,
)
from django.contrib.auth.models import User
from rest_framework_simplejwt.tokens import RefreshToken
from django_otp.plugins.otp_totp.models import TOTPDevice
from rest_framework.views import APIView
from rest_framework import status
from .serializers import RegisterSerializer
from rest_framework.parsers import JSONParser
from django.db.models import Q
import random
import string
from drf_yasg.utils import swagger_auto_schema


class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_description="Register a new user")
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        user = User.objects.get(username=response.data["username"])
        TOTPDevice.objects.create(user=user, name="default")

        # Generate JWT token for the new user
        refresh = RefreshToken.for_user(user)
        response.data["refresh"] = str(refresh)
        response.data["access"] = str(refresh.access_token)

        return response


class UserListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]

    @swagger_auto_schema(operation_description="List all users (Admin only)")
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class PasswordViewSet(viewsets.ModelViewSet):
    queryset = Password.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        # Filter passwords by the authenticated user
        return self.queryset.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.action in ["create", "update", "partial_update"]:
            return PasswordCreateSerializer
        return PasswordSerializer

    def perform_create(self, serializer):
        # Associate the password with the authenticated user
        serializer.save(user=self.request.user)

    @swagger_auto_schema(operation_description="Search passwords")
    @action(detail=False, methods=["get"], url_path="search")
    def search(self, request):
        query = request.query_params.get("q", "")
        if query:
            passwords = self.get_queryset().filter(Q(label__icontains=query))
        else:
            passwords = self.get_queryset()
        serializer = self.get_serializer(passwords, many=True)
        return Response(serializer.data)


class GeneratePasswordView(APIView):
    @swagger_auto_schema(operation_description="Generate a strong random password")
    def get(self, request, format=None):
        length = random.randint(12, 36)
        characters = string.ascii_letters + string.digits + string.punctuation
        password = "".join(random.choice(characters) for i in range(length))
        return Response({"password": password}, status=status.HTTP_200_OK)


class ModifyPasswordView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(operation_description="Modify an existing password")
    def patch(self, request, pk, format=None):
        try:
            password_entry = Password.objects.get(pk=pk, user=request.user)
        except Password.DoesNotExist:
            return Response(
                {"detail": "Password not found."}, status=status.HTTP_404_NOT_FOUND
            )

        serializer = PasswordSerializer(password_entry, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TOTPSetupView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(operation_description="Setup TOTP for user")
    def get(self, request):
        user = request.user
        device = TOTPDevice.objects.filter(user=user, name="default").first()
        if not device:
            device = TOTPDevice.objects.create(user=user, name="default")

        # Generate TOTP URL
        totp_url = device.config_url

        return Response({"totp_url": totp_url}, status=status.HTTP_200_OK)


class TOTPVerifyView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(operation_description="Verify TOTP token")
    def post(self, request):
        user = request.user
        device = TOTPDevice.objects.filter(user=user, name="default").first()
        if not device:
            return Response(
                {"detail": "No TOTP device found."}, status=status.HTTP_404_NOT_FOUND
            )

        token = request.data.get("token")
        if not token:
            return Response(
                {"detail": "Token not provided."}, status=status.HTTP_400_BAD_REQUEST
            )

        if device.verify_token(token):
            return Response(
                {"detail": "TOTP token is valid."}, status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"detail": "Invalid TOTP token."}, status=status.HTTP_400_BAD_REQUEST
            )


class ExportPasswordsView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        operation_description="Export passwords for the authenticated user"
    )
    def get(self, request):
        user = request.user
        passwords = Password.objects.filter(user=user)
        serializer = PasswordSerializer(passwords, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ImportPasswordsView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(
        operation_description="Import passwords for the authenticated user"
    )
    def post(self, request):
        user = request.user
        data = JSONParser().parse(request)
        for item in data:
            item["user"] = user.id
            serializer = PasswordSerializer(data=item)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(
            {"detail": "Passwords imported successfully."},
            status=status.HTTP_201_CREATED,
        )
