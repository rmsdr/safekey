from django.test import TestCase
from django.contrib.auth.models import User
from ..models import Password


class PasswordModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser", password="password123"
        )

    def test_password_creation(self):
        password = Password.objects.create(
            user=self.user,
            label="Test Label",
            encrypted_password="encrypted_password_here",
        )
        self.assertEqual(password.label, "Test Label")
        self.assertEqual(password.encrypted_password, "encrypted_password_here")
        self.assertEqual(password.user, self.user)
