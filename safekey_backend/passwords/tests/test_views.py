from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APIClient
from ..models import Password
from django_otp.plugins.otp_totp.models import TOTPDevice


class PasswordViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="password123"
        )
        self.password = Password.objects.create(
            user=self.user,
            label="Test Label",
            encrypted_password="encrypted_password_here",
        )
        self.client.force_authenticate(user=self.user)

    def test_list_passwords(self):
        response = self.client.get(reverse("password-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_create_password(self):
        data = {
            "label": "New Label",
            "encrypted_password": "new_encrypted_password_here",
        }
        response = self.client.post(reverse("password-list"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Password.objects.count(), 2)

    def test_update_password(self):
        data = {"label": "Updated Label"}
        response = self.client.patch(
            reverse("password-detail", args=[self.password.id]), data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.password.refresh_from_db()
        self.assertEqual(self.password.label, "Updated Label")

    def test_delete_password(self):
        response = self.client.delete(
            reverse("password-detail", args=[self.password.id])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Password.objects.count(), 0)


class RegisterViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_register_user(self):
        data = {
            "username": "newuser",
            "email": "newuser@example.com",
            "password": "newpassword123",
        }
        response = self.client.post(reverse("register"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(User.objects.filter(username="newuser").exists())


class TOTPSetupViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="password123"
        )
        self.client.force_authenticate(user=self.user)

    def test_totp_setup(self):
        response = self.client.get(reverse("totp-setup"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("totp_url", response.data)


class TOTPVerifyViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="password123"
        )
        self.client.force_authenticate(user=self.user)
        self.totp_device = TOTPDevice.objects.create(user=self.user, name="default")

    def test_totp_verify(self):
        # Generate a token from the TOTP device
        token = self.totp_device.generate_token()
        response = self.client.post(reverse("totp-verify"), {"token": token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["detail"], "TOTP token is valid.")
