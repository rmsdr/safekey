from django.test import TestCase
from django.contrib.auth.models import User
from ..serializers import PasswordSerializer


class PasswordSerializerTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="testuser", password="password123"
        )
        self.password_data = {
            "user": self.user.id,
            "label": "Test Label",
            "encrypted_password": "encrypted_password_here",
        }
        self.serializer = PasswordSerializer(data=self.password_data)

    def test_serializer_valid(self):
        self.assertTrue(self.serializer.is_valid())

    def test_serializer_fields(self):
        self.serializer.is_valid()
        self.assertEqual(self.serializer.validated_data["label"], "Test Label")
        self.assertEqual(
            self.serializer.validated_data["encrypted_password"],
            "encrypted_password_here",
        )
        self.assertEqual(self.serializer.validated_data["user"], self.user)
